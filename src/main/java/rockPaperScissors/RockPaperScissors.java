package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while(true)
        {
            System.out.printf("Let's play round %d\n", roundCounter);
            String user_input = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            java.util.Random random = new java.util.Random();
            int rand = random.nextInt(rpsChoices.size());
            String computer_choice = rpsChoices.get(rand);
        
            if(!rpsChoices.contains(user_input)){System.out.printf("I do not understand %s. Could you try again?\n", user_input);}
            else
            {
                System.out.printf("Human chose %s, computer chose %s. ", user_input, computer_choice);
                if(computer_choice.equals(user_input)){System.out.println("It's a tie !");}
                else if(computer_choice.equals("rock")){
                    if(user_input.equals("paper")){humanScore += 1; System.out.println("Player wins!");}
                    else{computerScore += 1; System.out.println("Computer wins!");} 
                }
                else if(computer_choice.equals("paper")){
                    if(user_input.equals("scissors")){humanScore += 1; System.out.println("Player wins!");}
                    else{computerScore += 1; System.out.println("Computer wins!");}   
                }
                else{
                    if(!user_input.equals("paper")){humanScore += 1; System.out.println("Player wins!");}
                    else{computerScore += 1; System.out.println("Computer wins!");} 
                }
                System.out.printf("Score: human %d, computer %d \n",humanScore,computerScore);
                if(readInput("Do you wish to continue playing? (y/n)?").equals("n")){
                    System.out.printf("Bye bye :)"); break;
                }
                roundCounter += 1;
            }

        }
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
